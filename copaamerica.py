# from teams import all_teams
from random import choice
from string import ascii_uppercase

all_teams = []
teams = []
groups = []
dates = []
table = []
third = []
first_place = None
second_place = None
third_place = None

def team_registration():
    team = {
        'information':{},
        'records':{}
    }

    print('Registro de Equipo')
    print()
    team['information']['code'] = input('Código de Equipo: ')
    team['information']['name'] = input('Nombre del Equipo: ')
    team['information']['country'] = input('País de Origen: ')
    print()
    team['information']['players'] = player_registration()

    all_teams.append(team)

def player_registration():
    players = []

    print('Registro de Jugadores')
    print()
    for i in range(2):
        player = {}
        player['position'] = input('Posición [PO/DF/MC/DD]: ')
        player['name'] = input('Nombre: ')
        print()
        players.append(player)

    print('Registro de Jugadores Completo.')
    input()
    return players

def teams_info():
    print('EQUIPOS REGISTRADOS\n')
    for team in all_teams:
        print(all_teams.index(team)+1, end="\t")
        print(team['information']['code'], end="\t")
        print(team['information']['name'])

def teams_define():
    for team in all_teams:
        teams.append({'name':team['information']['country'],'availability':True,'game_played':[]})

def groups_makers(teams):
    for i in range(int(len(teams)/4)):
            # print(f'GRUPO {ascii_uppercase[i]}')
            groups.append({'group':ascii_uppercase[i],'teams':[]})
            group = []
            while len(group) < 4:
                x = choice(teams)
                if not(x in group):
                    group.append(x)
                    teams.remove(x)
            # for team in group:
            #     print(team['name'])
                
            print()
            groups[i]['teams'] = group

    return groups

def print_groups():
    for group in groups:
        print('GRUPO {}'.format(group['group']))
        for team in group['teams']:
            print(team['name'])
        print()

def games_date(groups):

    for i in range(3):
        dates.append({'date':i+1,'games':[]})
        for group in groups:
            for team in group['teams']:
                team['availability'] = True

            for team in range(int(len(group['teams'])/2)):
                while True:
                    local = choice(group['teams'])
                    if local['availability'] == True:
                        local['availability'] = False
                        break
                while True:
                    visitor = choice(group['teams'])
                    if visitor['availability'] == True and not(visitor['name'] in local['game_played']):
                        visitor['availability'] = False
                        break
                local['game_played'].append(visitor['name'])
                visitor['game_played'].append(local['name'])

                dates[i]['games'].append({'local':local,'visitor':visitor})

def print_dates():
    for date in dates:
        print('FECHA {}'.format(date['date']))
        for game in date['games']:
            print(game['local']['name'],'vs',game['visitor']['name'])
        print()

def print_players():
    team_name = input("\nIngrese el país a consultar: ")
    for team in all_teams:
        if team['information']['country'] == team_name or team['information']['code'] == team_name:
            print(team['information']['name'])
            print('País: {}'.format(team['information']['country']))
            print('JUGADORES\n')
            for player in team['information']['players']:
                print('[{}] - {}'.format(player['position'],player['name']))

def points_calculator(won, tied):
    return (won*3)+(tied*1)

def group_stage():
    for date in dates:
        print('FECHA {}'.format(date['date']))
        for game in date['games']:
            print('Partido: {} vs {}'.format(game['local']['name'],game['visitor']['name']))
            
            for team in all_teams:
                if team['information']['country'] == game['local']['name']:
                    team['records']['PJ'] += 1
                if team['information']['country'] == game['visitor']['name']:
                    team['records']['PJ'] += 1

            local = int(input('{}: '.format(game['local']['name'])))
            visitor = int(input('{}: '.format(game['visitor']['name'])))
            if local > visitor:
                print('Gana {}.'.format(game['local']['name']))
                for team in all_teams:
                    if team['information']['country'] == game['local']['name']:
                        team['records']['G'] += 1
                    if team['information']['country'] == game['visitor']['name']:
                        team['records']['P'] += 1
            elif local == visitor:
                print('Empate.')
                for team in all_teams:
                    if team['information']['country'] == game['local']['name']:
                        team['records']['E'] += 1
                    if team['information']['country'] == game['visitor']['name']:
                        team['records']['E'] += 1
            else:
                print('Gana {}'.format(game['visitor']['name']))
                for team in all_teams:
                    if team['information']['country'] == game['local']['name']:
                        team['records']['P'] += 1
                    if team['information']['country'] == game['visitor']['name']:
                        team['records']['G'] += 1
            
            for team in all_teams:
                if team['information']['country'] == game['local']['name']:
                    team['records']['GF'] += local
                    team['records']['GC'] += visitor
                    team['records']['DF'] = team['records']['GF'] - team['records']['GC']
                    team['records']['Pts'] = points_calculator(team['records']['G'],team['records']['E'])
                    # print(team['information']['country'])
                    # print(team['records'])
                if team['information']['country'] == game['visitor']['name']:
                    team['records']['GF'] += visitor
                    team['records']['GC'] += local
                    team['records']['DF'] = team['records']['GF'] - team['records']['GC']
                    team['records']['Pts'] = points_calculator(team['records']['G'],team['records']['E'])
                    # print(team['information']['country'])
                    # print(team['records'])
            
            print()

def position_table():
    for team in all_teams:
        table.append(
            {
                'team':{
                    'code':team['information']['country'],
                    'G':team['records']['G'],
                    'E':team['records']['E'],
                    'P':team['records']['P'],
                    'GF':team['records']['GF'],
                    'GC':team['records']['GC'],
                    'DF':team['records']['DF'],
                    'Pts':team['records']['Pts']
                }
        })
    
    table.sort(key=lambda t: t['team']['Pts'],reverse=True)

def update_position_table():
    table = []
    for team in all_teams:
        table.append(
            {
                'team':{
                    'code':team['information']['country'],
                    'G':team['records']['G'],
                    'E':team['records']['E'],
                    'P':team['records']['P'],
                    'GF':team['records']['GF'],
                    'GC':team['records']['GC'],
                    'DF':team['records']['DF'],
                    'Pts':team['records']['Pts']
                }
        })
    
    table.sort(key=lambda t: t['team']['Pts'],reverse=True)

def print_position_table():
        print('G\tE\tP\tGF\tGC\tDF\tPts\tEQUIPO\n')
        for team in table:
            
            print(team['team']['G'], end='\t')
            print(team['team']['E'], end='\t')
            print(team['team']['P'], end='\t')
            print(team['team']['GF'], end='\t')
            print(team['team']['GC'], end='\t')
            print(team['team']['DF'], end='\t')
            print(team['team']['Pts'], end='\t')
            print(team['team']['code'])

def quarter_finals():
    print('CUARTOS DE FINAL\n')
    winners = []
    quarter = []

    for i in range(8):
        quarter.append(table[i]['team']['code'])

    i = 0
    j= 1
    for games in range(4):
        while True:
            print(f'{quarter[i]} vs {quarter[j]}')
            for team in all_teams:
                        if team['information']['country'] == quarter[i]:
                            team['records']['PJ'] += 1
                        if team['information']['country'] == quarter[j]:
                            team['records']['PJ'] += 1
            local = int(input(f'{quarter[i]}: ' ))
            visitor = int(input(f'{quarter[j]}: '))
            if local > visitor:
                print(f'Gana {quarter[i]}.')
                winners.append(quarter[i])
                for team in all_teams:
                    if team['information']['country'] == quarter[i]:
                        team['records']['G'] += 1
                    if team['information']['country'] == quarter[j]:
                        team['records']['P'] += 1
                break
            elif local == visitor:
                print('No puede haber empate.')
            else:
                print(f'Gana {quarter[j]}')
                winners.append(quarter[j])
                for team in all_teams:
                    if team['information']['country'] == quarter[i]:
                        team['records']['P'] += 1
                    if team['information']['country'] == quarter[j]:
                        team['records']['G'] += 1
                break
        for team in all_teams:
            if team['information']['country'] == quarter[i]:
                team['records']['GF'] += local
                team['records']['GC'] += visitor
                team['records']['DF'] = team['records']['GF'] - team['records']['GC']
                team['records']['Pts'] = points_calculator(team['records']['G'],team['records']['E'])
                # print(team['information']['country'])
                # print(team['records'])
            if team['information']['country'] == quarter[j]:
                team['records']['GF'] += visitor
                team['records']['GC'] += local
                team['records']['DF'] = team['records']['GF'] - team['records']['GC']
                team['records']['Pts'] = points_calculator(team['records']['G'],team['records']['E'])
                # print(team['information']['country'])
                # print(team['records'])
        i+=2
        j+=2
        print()

    return winners

def semifinal(result):
    print('SEMIFINALES\n')
    winners = []
    
    i=0
    j=1

    for game in range(2):
        while True:
            print(f'{result[i]} vs {result[j]}')
            for team in all_teams:
                        if team['information']['country'] == result[i]:
                            team['records']['PJ'] += 1
                        if team['information']['country'] == result[j]:
                            team['records']['PJ'] += 1
            local = int(input(f'{result[i]}: ' ))
            visitor = int(input(f'{result[j]}: '))
            if local > visitor:
                print(f'Gana {result[i]}.')
                winners.append(result[i])
                third.append(result[j])
                for team in all_teams:
                    if team['information']['country'] == result[i]:
                        team['records']['G'] += 1
                    if team['information']['country'] == result[j]:
                        team['records']['P'] += 1
                break
            elif local == visitor:
                print('No puede haber empate.')
            else:
                print(f'Gana {result[j]}')
                winners.append(result[j])
                third.append(result[i])
                for team in all_teams:
                    if team['information']['country'] == result[i]:
                        team['records']['P'] += 1
                    if team['information']['country'] == result[j]:
                        team['records']['G'] += 1
                break

        i+=2
        j+=2
        print()
    
    return winners

def third_places(third):
    print('TERCER LUGAR')
    print(f'{third[0]} vs {third[1]}')
    while True:
        for team in all_teams:
            if team['information']['country'] == third[0]:
                team['records']['PJ'] += 1
            if team['information']['country'] == third[1]:
                team['records']['PJ'] += 1
        local = int(input(f'{third[0]}: ' ))
        visitor = int(input(f'{third[1]}: '))
        if local > visitor:
            print(f'Gana {third[0]}.')
            third_place = third[0]
            
            # for team in all_teams:
            #     if team['information']['country'] == finals[0]:
            #         team['records']['G'] += 1
            #     if team['information']['country'] == finals[1]:
            #         team['records']['P'] += 1
            break
        elif local == visitor:
            print('No puede haber empate.')
        else:
            print(f'Gana {third[1]}')
            third_place = third[1]

            # for team in all_teams:
            #     if team['information']['country'] == finals[0]:
            #         team['records']['P'] += 1
            #     if team['information']['country'] == finals[1]:
            #         team['records']['G'] += 1
            break
    print()

def final(finals):
    print('FINAL\n')
    print(f'{finals[0]} vs {finals[1]}')
    
    while True:
        for team in all_teams:
            if team['information']['country'] == finals[0]:
                team['records']['PJ'] += 1
            if team['information']['country'] == finals[1]:
                team['records']['PJ'] += 1
        local = int(input(f'{finals[0]}: ' ))
        visitor = int(input(f'{finals[1]}: '))
        if local > visitor:
            print(f'Gana {finals[0]}.')
            first_place = finals[0]
            second_place = finals[1]
            for team in all_teams:
                if team['information']['country'] == finals[0]:
                    team['records']['G'] += 1
                if team['information']['country'] == finals[1]:
                    team['records']['P'] += 1
            break
        elif local == visitor:
            print('No puede haber empate.')
        else:
            print(f'Gana {finals[1]}')
            first_place = finals[1]
            second_place = finals[0]
            for team in all_teams:
                if team['information']['country'] == finals[0]:
                    team['records']['P'] += 1
                if team['information']['country'] == finals[1]:
                    team['records']['G'] += 1
            break
    print()

def start_menu():
    print('COPA AMERICA')
    print('1. Registrar equipos y jugadores.')
    print('2. Comenzar Copa America')
    print('3. Información de Jugadores')
    print('0. Salir')
    try:
        opc = int(input('Opcion: '))
    except ValueError:
        print('Debes ingresar un número.')
    
    return opc

def game_over():
    print('COPA AMERICA')
    print(f'Primer Lugar: {first_place}')
    print(f'Segundo Lugar: {second_place}')
    print(f'Tercer Lugar: {third_place}')
    print()
    update_position_table()
    print_position_table()
    input()

#######################################################

if __name__ == '__main__':
    copa_america = True
    while copa_america:
        opc = start_menu()
        if opc == 1:
            team_registration()
        elif opc == 2:
            if len(all_teams) > 0:
                teams_define()
                if len(teams) % 4 == 0 and len(teams)/4 > 1:
                    input('Haciendo Grupos...')
                    groups_makers(teams)
                    print_groups()
                    input('Generando Fechas...')
                    games_date(groups)
                    print_dates()
                    group_stage()
                    table = []
                    position_table()
                    print_position_table()
                    while True:
                        print('1. Continuar a Fase Final.')
                        print('2. Mostrar Tabla de Posiciones.')
                        try:
                            opc = int(input('Opción: '))
                        except ValueError:
                            print('debes ingresar un número.')
                        
                        if opc == 1:
                            result = quarter_finals()
                            finals = semifinal(result)
                            third_places(third)
                            final(finals)
                            game_over()
                            copa_america = False
                            break
                        elif opc == 2:
                            print_position_table()
                        else: 
                            print('Opción incorrecta.')

                else:
                    print('No hay sufucuentes equipos registrados para comenzar la Copa América.')
            else:
                print('No hay Equipos registrados.')
        elif opc == 3:
            if len(all_teams) > 0:
                print_players()
            else:
                print('No hay equipos registrados.')
        elif opc == 0:
            copa_america = False